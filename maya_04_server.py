import socket
LISTENING_PORT = 21623
TYPE_MSG_ONE = "you choose option 1"
TYPE_MSG_TWO = "you choose option 2"
TYPE_MSG_THREE = "you choose option 3"
TYPE_MSG_FOUR = "you choose option 4"
TYPE_MSG_FIVE = "you choose option 5"
TYPE_MSG_SIX = "you choose option 6"
TYPE_MSG_SEVEN = "you choose option 7"


def create_server():
    """
    The function creates a server socket that the client connects to.
    :return: none
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as listening_sock:
        from_client_dict = {
            "option1;": TYPE_MSG_ONE,
            "option2;user_input=": TYPE_MSG_TWO,
            "option3;user_input=": TYPE_MSG_THREE,
            "option4;user_input=": TYPE_MSG_FOUR,
            "option5;user_input=": TYPE_MSG_FIVE,
            "option6;user_input=": TYPE_MSG_SIX,
            "option7;user_input=": TYPE_MSG_SEVEN
        }
        from_client_msg = ""
        # creating listening socket
        server_address = ('', LISTENING_PORT)
        listening_sock.bind(server_address)
        listening_sock.listen(1)
        client_soc, client_address = listening_sock.accept()
        # sending the welcome message to the client.
        msg_to_client = "Welcome to my Pink Floyd program!"
        client_soc.sendall(msg_to_client.encode())
        # while the user doesn't choose the 8th option, the server will continue receiving messages from the client.
        while "option8" not in from_client_msg:
            # receiving message from the client.
            from_client_msg = client_soc.recv(1024)
            from_client_msg = from_client_msg.decode()
            # using the from_client_dict, sending a message to the client.
            response_to_client = from_client_dict[from_client_msg]
            client_soc.sendall(response_to_client.encode())
        if "option8" in from_client_msg:
            quit()


def main():
    create_server()


if __name__ == "__main__":
    main()
