def print_open_screen():
  #prints the open screen.
  MAX_TRIES = 6
  print("""
  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/""")
  print(MAX_TRIES)

def print_hangman(num_of_tries):
  """
  The function recives a number that presents the amount of tries, and prints the matching picture.
  :param num_of_tries: number of failed tries by far.
  :type num_of_tries: int.
  :return: none.
  :rtype: none.
  """
  HANGMAN_PHOTOS = {'picture 1': '\nx-------x', 'picture 2': '\nx-------x\n|\n|\n|\n|\n|', 'picture 3': '\nx-------x\n|       |\n|       0\n|\n|\n|', 'picture 4': '\nx-------x\n|       |\n|       0\n|       |\n|\n|', 'picture 5': '\nx-------x\n|       |\n|       0\n|      /|\\\n|\n|', 'picture 6': '\nx-------x\n|       |\n|       0\n|      /|\\\n|      /\n|', 'picture 7': '\nx-------x\n|       |\n|       0\n|      /|\\\n|      / \\\n|'}
 
  if num_of_tries == 1:
    print(HANGMAN_PHOTOS['picture 1'])
  elif num_of_tries == 2:
    print(HANGMAN_PHOTOS['picture 2'])
  elif num_of_tries == 3:
    print(HANGMAN_PHOTOS['picture 3'])
  elif num_of_tries == 4:
    print(HANGMAN_PHOTOS['picture 4'])
  elif num_of_tries == 5:
    print(HANGMAN_PHOTOS['picture 5'])
  elif num_of_tries == 6:
    print(HANGMAN_PHOTOS['picture 6'])
  elif num_of_tries == 7:
    print(HANGMAN_PHOTOS['picture 7'])

def show_hidden_word(secret_word, old_letters_guessed):
  """
  The function recives a secret word and a list of letters that have been guessed. it returns a string that shows which characters from the old letters guessed are in the string.
  :param secret_word: the word to check.
  :param old_letters_guessed: the list of the letters that have been guessed.
  :type secret_word: string.
  :type old_letters_guessed: list.
  :return: a string that shows which of the letters from the list are in the secret_word.
  :rtype: string.
  """
  i = 0
  secret_word = list(secret_word)
  for letter in secret_word:
    if not(letter in old_letters_guessed):
      secret_word[i] = "_"
    i += 1
  str_guess = ' '.join(secret_word)
  print(str_guess)

def choose_word(file_path, index):
  file_path = open(file_path, 'r')
  read_file = file_path.read()
  words = read_file.split()
  words = " ".join(sorted(set(words), key = words.index))
  list_words = list(words.split(" "))
  read_file = list(read_file.split(" "))
  length_og_file = len(read_file)
  index = index - 1
  if length_og_file == 1:
    index = 0
  if index > length_og_file:
    index = abs(length_og_file - index)
  secret_word = read_file[index]
  return secret_word
  

def check_valid_input(letter_guessed, old_letters_guessed):
  """Checks if the input is valid or not and if it was guessed before.
  :param letter_guessed: the input letter.
  :param old_letter_guessed: a list of the letters guessed.
  :type letter_guessed: string
  :type old_letter_guessed: list
  :return: True or False.
  :rtype: boolean.
  """
  length = len(letter_guessed)
  is_alpha = letter_guessed.isalpha()
  letter_guessed = letter_guessed.lower()
  if length == 1 and is_alpha == True and not (letter_guessed in old_letters_guessed):
      return True
  else:
       return False

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
  """Checks if the input is valid or not and according to that adds/ not to the letters already guessed.
  :param letter_guessed: the input letter.
  :param old_letter_guessed: a list of the letters guessed.
  :type letter_guessed: string
  :type old_letter_guessed: list
  :return: True or False.
  :rtype: boolean.
  """
  count_tries = 0
  num_of_tries = 0
  secret_word = ''
  length = len(letter_guessed)
  is_alpha = letter_guessed.isalpha()
  letter_guessed = letter_guessed.lower()
  if (length == 1 and is_alpha == True) and not (letter_guessed in old_letters_guessed):
    old_letters_guessed += letter_guessed
    return True
  else:
    print("X")
    letter_string = sorted(old_letters_guessed)
    letter_string = " -> ".join(letter_string)
    print(letter_string)
    return False

def check_win(secret_word, old_letters_guessed):
  """
  The function recives a secret word and a list of letters that have been guessed. it returns True if all of the guessed letter are on the list, otherwise it'll return False.
  :param secret_word: the word to check.
  :param old_letters_guessed: the list of the letters that have been guessed.
  :type secret_word: string.
  :type old_letters_guessed: list.
  :return: True / False.
  :rtype: boolean.
  """
  i = 0
  secret_word = list(secret_word)
  for letter in secret_word:
    if not(letter in old_letters_guessed):
      secret_word[i] = "_"
    i += 1
  str_guess = ' '.join(secret_word)
  if "_" in str_guess:
    return False
  else:
    return True


if __name__ == "__main__":
  count_tries = 1
  old_letters_guessed = []
  print_open_screen()
  file_path = input("Enter file path: ")
  index = int(input("Enter index: "))
  print("\nLet’s start!\n")
  print(print_hangman(1))
  secret_word = choose_word(file_path, index)
  while count_tries <= 7:
    print(show_hidden_word(secret_word, old_letters_guessed))
    input_guess = input("Guess a letter: ")
    old_letters_guessed = try_update_letter_guessed(input_guess, old_letters_guessed)
    if try_update_letter_guessed(input_guess, old_letters_guessed):
      count_tries += 1
    if(check_win(secret_word, old_letters_guessed)):
      print("WIN")
      break
  else:
    print("LOSE")